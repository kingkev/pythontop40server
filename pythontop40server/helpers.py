# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Modifications copyright (C) 2015 Kevin Ndung'u

from pythontop40server.config import CACHE_SECONDS
from pythontop40server import log, mongo
from pythontop40server.errors import Top40ScrapingError
from html.parser import HTMLParseError
import bs4
import arrow
import re
import requests

__author__ = 'Danny Goodall'


def get_chart_or_cache(chart_type, stale_seconds=CACHE_SECONDS):
    """Read chart from Mongo and determine if it is stale. If so, replace it.

    Args:
        chart_type (:py:class:`str`) - The type of chart either 'singles' or 'albums'

    Returns:
        chart_dict (:py:class:`dict`) - The chart described as a dictionary
    """
    chart_dict = retrieve_chart(chart_type)

    # If the chart_dict has come back as None, then we have no chart in MongoDB. This means we need to get the
    # chart from the BBC site
    mongo_empty = chart_dict is None

    if mongo_empty:
        # Scrape the BBC web site for the chart details
        chart_dict = scrape_bbc_page(chart_type)

    # Grab the date that the chart was retrieved from the BBC site
    date_of_chart = arrow.get((chart_dict['retrieved']))

    # When will the chart be stale
    stale_time = date_of_chart.replace(seconds=stale_seconds)
    time_now = arrow.utcnow()

    # Is the time of the chart past the stale seconds span?
    if time_now > stale_time:
        log.debug("Cache was stale - ({} > {})".format(time_now,stale_time))
        # The cache is stale, so we need to scrape the BBC site
        chart_dict = scrape_bbc_page(chart_type)

        #Now store this chart_dict back to the cache
        store_chart(chart_type, chart_dict)
    else:
        log.debug("  Retrieved from cache - ({} < {})".format(time_now, stale_time))

    return chart_dict


def retrieve_chart(chart_type):
    """Retrieve this chart type from MongoDB

    Look for this chart type in the database and return it
    """

    # Let's get the correct collection, based upon our chart type (singles or albums)
    chart_collection = mongo.db[chart_type]

    # Let's grab the find document in the collection
    chart_dict = chart_collection.find_one()

    return chart_dict


def store_chart(chart_type, chart_dict):
    """Store this chart in MongoDB

    Look for this chart in the database and overwrite it

    Args:
        chart_type:
    :param chart_dict:
    :return:
    """

    # Let's get the correct collection, based upon our chart type (singles or albums)
    chart_collection = mongo.db[chart_type]

    # Get the chart from MongoDB so that we can updated it - we are only storing one document in each collection
    # so the document needs to be overwritten each time - hence we need to retrieve it first.
    current_chart_dict = retrieve_chart(chart_type)

    # IF one was returned from MongoDB, then update the chart document from the chart_dict we want to store,
    # otherwise if there is no chart in MongoDB, then set the chart document to be the chart_dict we want to store
    if current_chart_dict is not None:
        current_chart_dict.update(**chart_dict)
    else:
        current_chart_dict = chart_dict

    # Update the chart_document and put it back into MongoDB, or insert if this chart_dict is new
    chart_collection.save(current_chart_dict)

    return chart_dict


def strip_number_suffix(string):
    return re.sub(r'(\d)(st|nd|rd|th)', r'\1', string)

def get_change_dict(position, previous):
    """Return a dictionary that describes the change since last week using Ben Major's API format.

    One change from Ben Major's format is that new entries will show as an "up" change and the actual and amount
    parts will be computed as if the single or album was at #41 the previous week. Therefore an entry that is straight
    in at number 1, will show an amount and actual move of 40 places.

    >>> change_dict(11,16)
    >>> {"direction": "up", "amount": 5. "actual": 5}

    >>> change_dict(11,0)
    >>> {"direction": "up", "amount": 30. "actual": 30}

    >>> change_dict(16,11)
    >>> {"direction": "down", "amount": 0, "actual": -5}

    >>> change_dict(11,11)
    >>> {"direction": "none", "amount": 0, "actual": 0}

    Args:
        position (:py:class:`int`) : The position in the chart 1-40
        previous (:py:class:`int`) : Last week's chart position

    Returns:
        change_dict (:py:class:`dict`) : A dictionary containing keys that describe the Change since last week
    """
    actual = (previous if previous else 41) - position
    amount = abs(actual)
    if actual > 0:
        direction="up"
    elif actual < 0:
        direction="down"
    else:
        direction="none"

    return dict(direction=direction, actual=actual, amount=amount)


def get_page_date(page_title):
    """Return the date of the chart by extracting it from the title

    The page title is passed in this format

    >>> page_title = "The Official UK Top 40 Albums Chart - 30th November 2014"

    The strategy is to split the string on the -

    >>> page_title.split("-")[1]
    >>> " 30th November 2014")

    Trim the leading space

    >>> page_title.lstrip()
    >>> "30th November 2014"

    Then remove the number suffix (th,st,nd,rd)

    >>> date_string = strip_number_suffix(page_title)
    >>> "30 November 2014"

    And finally to to parse the string and convert to a date

    >>> date_of_chart = arrow.get(date_string,["D MMMM YYYY", "DD MMMM YYYY"])
    >>> <Arrow [2014-11-30T00:00:00+00:00]>

    Args:
        page_title (:py:class:`str`) : The title extracted from the HTML page

    Returns
        date_of_chart (:py:class:`Arrow`) : The date of the chart as an Arrow date.
    """
    if not page_title:
        raise Top40ScrapingError("No title was found in the html document")
    if not "-" in page_title:
        raise Top40ScrapingError("Page title incorrectly formed.")

    # Split the page title on the - and take the right hand side
    date_string = page_title.split("-")[1].lstrip()
    # Remove the number suffix st, nd, rd, th
    date_string = strip_number_suffix(date_string)
    # Create a date format
    try:
        date_of_chart = arrow.get(date_string, ["D MMMM YYYY", "DD MMMM YYYY"])
    except RuntimeError:
        raise Top40ScrapingError("Couldn't parse the title to get the chart date.")

    return date_of_chart


def scrape_bbc_page(chart_type="albums"):
    """Scrape the relevant page on the BBC website and return chart information

    Looks for the relevant page, downloads it and extracts the chart information from the HTML. Returns the chart
    information as a dictionary in the format define by `Ben Major here <http://ben-major.co.uk/2013/12/uk-top-40-charts-api/>`_.

    >>> #IF THE ATTEMPT TO READ THE BBC SITE FAILS
    >>> scrape_bbc_page()
    >>> {}

    >>> scrape_bbc_page("singles")
    >>> {
    >>>     "date": 1386460800,
    >>>     "retrieved": 1386669718,
    >>>     "entries":
    >>>     [
    >>>        {
    >>>            "position": 1,
    >>>            "previousPosition": 1,
    >>>            "numWeeks": 2,
    >>>            "artist": "One Direction",
    >>>            "title": "Midnight Memories",
    >>>            "change":
    >>>            {
    >>>                "direction": "none",
    >>>                "amount": 0,
    >>>                "actual": 0
    >>>            }
    >>>        },
    >>>        .
    >>>        .
    >>>        .
    >>>    ]
    >>> }

    Used some of the logic for scraping a table from this page: http://stackoverflow.com/a/18966444/1300916

    The page being scraped has the following structure

    <html lang="en-gb">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <meta name="robots" content="noindex">
            <link rel="stylesheet" href="http://static.bbci.co.uk/radio/663/1.33/style/components/chart_print.css">
            <link rel="stylesheet" href="http://static.bbci.co.uk/radio/663/1.33/style/components/masthead_logos.css">
            <title>The Official UK Top 40 Singles Chart - 7th December 2014</title>
        </head>
        <body class="service-bbc_radio_one">
            <div class="masterbrand-logo"></div>
            <h1>The Official UK Top 40 Singles Chart - 7th December 2014</h1>
            <table border="1" cellpadding="3" cellspacing="0">
                <tbody>
                    <tr>
                        <th>Position</th>
                        <th>Status</th>
                        <th>Previous</th>
                        <th>Weeks</th>
                        <th>Artist</th>
                        <th>Title</th>
                    </tr>
                        <tr>
                            <td>1</td>
                            <td>up 3</td>
                            <td>4</td>
                            <td>24</td>
                            <td>Ed Sheeran</td>
                            <td>Thinking Out Loud</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>new</td>
                            <td></td>
                            <td>1</td>
                            <td>Union J</td>
                            <td>You Got It All</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>down 2</td>
                            <td>1</td>
                            <td>2</td>
                            <td>Take That</td>
                            <td>These Days</td>
                        </tr>
                        .
                        .
                        .
                       <tr>
                            <td>40</td>
                            <td>down 4</td>
                            <td>36</td>
                            <td>2</td>
                            <td>Beyoncé</td>
                            <td>7/11</td>
                        </tr>
                </tbody>
            </table>
        </body>
    </html>

    Args:
        page_type (:py:class:`str`): Either "albums" or "singles" - defaults to albums. This defines the type of chart
            that is requested.

    Returns:
        chart_info (:py:class:`dict`): A dictionary that describes the chart information
    """

    # Create a page template for the BBC site. Insert either singles or albums based on chart_type
    page = "http://www.bbc.co.uk/radio1/chart/{}/print".format(chart_type)

    # Create a list of the columns that will be enountered and their corresponding names and types
    unpacking_list = [
        ("position", int),
        ("status", str),
        ("previousPosition", int),
        ("numWeeks", int),
        ("artist", str),
        ("title", str)
    ]

    # Initialise our containers
    chart_dict = {}
    entries = []

    # There are many errors that can occur whilst we're trying to get the stuff from the BBC site
    # Let's catch them and then re-raise them as our own Top40ScrapingError
    try:
        html_text = requests.get(page)
        html_text.encoding = 'utf-8'
    except (requests.exceptions.ConnectionError, requests.exceptions.ConnectTimeout) as e:
        raise Top40ScrapingError("A connection error or connection timeout error occurred.") from e
    except requests.exceptions.HTTPError as e:
        raise Top40ScrapingError("An HTTP error was returned from the remote server") from e

    # We could get parse errors in the HTML, so let's raise a Top40ScrapingError if we do
    try:
        soup = bs4.BeautifulSoup(html_text.text)
    except (UnicodeEncodeError, KeyError, AttributeError, HTMLParseError) as e:
        raise Top40ScrapingError("Couldn't parse the text from "+page) from e

    # Now grab the date of the chart from the page title
    page_date = get_page_date(page_title=soup.find('title').string)
    todays_date = arrow.utcnow()

    # Find all table rows in the HTML document
    table_rows = soup.find_all("tr")
    if not table_rows:
        return chart_dict

    # Loop through the rows in the table - ignoring the header row
    for table_row in table_rows[1:]:
        entry_dict = {}
        table_data = table_row.find_all("td")

        # Now unpack the table_data and convert if necessary
        for column, column_info in enumerate(unpacking_list):
            # Set the dictionary key and conversion class to use
            dict_key, conversion_type = column_info

            # Set the column string or initialise an equivalent type if it is None
            column_string = table_data[column].string if table_data[column].string else conversion_type()

            # Assign the value from the web table to our entry dictionary, converting it as we go
            entry_dict[dict_key] = conversion_type(column_string)

        #Compute the change dict, based on this entry
        change_dict = get_change_dict(entry_dict["position"], entry_dict["previousPosition"])

        #Store the change dict away in the "change" key of our entry dict
        entry_dict["change"] = change_dict

        # Add the entry dictionary to the chart entries list - but remove the status key first as this isn't part of the
        # Booby model.
        entries.append(entry_dict)

    # Finished processing the rows, so now fill the container with what we saw
    chart_dict["date"] = page_date.timestamp
    chart_dict["retrieved"] = todays_date.timestamp
    chart_dict["entries"] = entries

    return chart_dict
