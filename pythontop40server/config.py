# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'Danny Goodall'
from pythontop40server.utils import env_value

DEBUG=env_value("DEBUG",False)

# Are we running on the Heroku platform (Requires an environment variable to be set on Heroku first!)
ON_HEROKU = env_value("ON_HEROKU", False)

# Configure the URI to connect to our MongDB instance. If we're on Heroku then it will be passed to us, if
# not then we have to set it separately.
MONGO_URI = env_value("MONGOHQ_URL", "mongodb://127.0.0.1:27017/app32330262")

# Get the number of seconds that a result can be in the cache before it is stale
CACHE_SECONDS = env_value("CACHE_SECONDS", 600)
