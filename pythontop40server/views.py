# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'Danny Goodall'

from pythontop40server import app, log, api
from flask import Flask, abort, render_template
from flask.ext import restful
from pythontop40server.helpers import get_chart_or_cache, retrieve_chart
from pythontop40 import Top40
from pythontop40.top40 import Chart
import arrow
from flask import render_template
from pythontop40server.errors import Top40ScrapingError

# Define the route for the index
@app.route('/')
def index():
    top40 = Top40(base_url="https://pythontop40server.herokuapp.com/v2/")
    singles_chart = top40.singles_chart
    albums_chart = top40.albums_chart

    return render_template(
        'index.html',
        **{
            "top40": top40,
            "singles_chart": singles_chart,
            "albums_chart": albums_chart,
            "singles_date": arrow.get(singles_chart.date).format("D MMMM YYYY HH:MMA"),
            "albums_date": arrow.get(albums_chart.date).format("D MMMM YYYY HH:MMA"),
            "singles_retrieved": arrow.get(singles_chart.retrieved).format("D MMMM YYYY HH:MMA"),
            "albums_retrieved": arrow.get(albums_chart.retrieved).format("D MMMM YYYY HH:MMA")
        }
    )

# Define the REST API Handler - only need the .get method
class ReturnChart(restful.Resource):
    def get(self, chart_type="albums", api_version="v2"):
        # Scrape the page and return the JSON document as a Python dictionary.
        # Except an error to occur as it seems that the BBC site suffers with transient errors. At some point in the
        # future I will build in a way of caching the results for a week

        log.debug("  Entered ReturnChart")

        # Validate that the parameters are in range
        if chart_type.lower() not in ['albums','singles']:
            abort(404)
        if api_version.lower() not in ['v1','v2']:
            abort(404)

        try:
            chart_dict = get_chart_or_cache(chart_type=chart_type)

            # The MongoDB routines will add an _id key to the dictionary if it has been used to store or
            # retrieve information from MongoDB. We should remove that before passing it to the chart routine
            chart_dict.pop("_id",None)

            # Let's just check that we can create a Booby model Chart from the response. If this raises an error
            # then we've got to assume that something broke in the scraping
            chart = Chart(**chart_dict)

            # Let's add a key to the chart_dict to say that this is a current (live or withing cache time) version
            # of the chart. This is used below to show that we are returning a non-current version of the chart
            # (i.e. being retrieved from the MongoDB) because we're not able to scrape the BBC site at the moment.

            if api_version == "v2":
                chart_dict["current"] = True

        except Top40ScrapingError as e:
            # OK so we got a scraping error from the BBC site, so let's return the version that we have in
            # the cache
            try:
                chart_dict = retrieve_chart(chart_type)
            except Exception as e:
                abort(500)

            # Let's add a key to the chart_dict to say that this is not a current (live or withing cache time) version
            # of the chart. This is probably because we're not able to scrape the BBC site at the moment.

            if api_version == "v2":
                chart_dict["current"] = False

            # Remove the MongoDB "_id" key before it is serialised to JSON
            chart_dict.pop("_id", None)

        # Return the dictionary and let Flask-RESTful do the conversion
        return chart_dict

api.add_resource(ReturnChart, '/<api_version>/<chart_type>/')

@app.errorhandler(500)
def server_error(e):
    return render_template('500.html'), 500

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@app.route('/500')
def server_400():
    abort(500)

@app.route('/404')
def server_404():
    abort(404)