# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

__author__ = 'Danny Goodall'

import os
import six

def get_env_value(env_var, default=None):
    value = os.getenv(env_var, None)
    # Return a tuple of the value and a bool that describes whether it was found in the environment
    return (value if value is not None else default, value is not None)

def cast_to_default(value, default):
    if isinstance(default, bool):
        value = value.lower() in ['on', 'true', '1', 'set']
    elif isinstance(default, int):
        value = int(float(value))
    elif isinstance(default, six.string_types):
        value = six.u(value)
    return value

def env_value(env_var, default):
    """
    Will return the value from the environment, cast to the same type as the default, or the default if not set
    """
    original_value, value_set = get_env_value(env_var, default)

    if not value_set:
        value = default
    else:
        value = cast_to_default(original_value, default)

    return value