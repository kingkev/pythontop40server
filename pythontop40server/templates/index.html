{% extends "base.html" %}
{% block title %}PythonTop40Server - providing API access to the UK Top 40 singles and albums for UK schools{% endblock %}

{% block navbar %}
<div class="navbar navbar-fixed-top">
  <!-- ... -->
</div>
{% endblock %}

{% block container_content %}
{{ super() }}
<div class="row">
    <div class="col-md-4">
        <h3>What is this?</h3>
        <p>
            <strong>PythonTop40Server</strong> exposes an API endpoint so that the UK Top40 singles and albums'
            charts can be accessed. It is designed to be used in UK schools as part of my
            <strong>Code-Further</strong> initiative. The source code for this server can be found
            <a href="http://www.bitbucket.org/dannygoodall/pythontop40server">here</a>.
        </p>
        <h4>Enpoints</h4>
        <p>
            JSON-based API endpoints are available to access the current singles and albums charts.
        </p>
        <p>
            <a href="/v1/singles">/v1/singles</a>
            <span class="label label-default">V1</span>
            <span class="label label-info">singles</span>
        </p>
        <p>
            <a href="/v1/albums">/v1/albums</a>
            <span class="label label-default">V1</span>
            <span class="label label-info">albums</span>
        </p>
        <p>
            <a href="/v2/singles">/v2/singles</a>
            <span class="label label-default">V2</span>
            <span class="label label-info">singles</span>
        </p>
        <p>
            <a href="/v2/albums">/v2/albums</a>
            <span class="label label-default">V2</span>
            <span class="label label-info">albums</span>
        </p>
        <h4>Accessing the API</h4>
        <p>
            Whilst any language that is capable of connecting to a JSON endpoint could be used, it is
            designed to be used with its sister Python module
            <a href="http://www.bitbucket.org/dannygoodall/pythontop40">PythonTop40</a>.
        </p>
        <p>
            This presents the developer with a series of high-level Python objects for accessing the chart
            information. A list of the Top 40 singles (such as the one shown on this page) could be accessed
            using the following code:
        </p><pre class="prettyprint"><code class="language-py">import pythontop40

top40 = pythontop40.Top40()

for entry in top40.singles_chart:
print( entry.position, entry.title... </code></pre>
        <h4>Credit</h4>
        <p>
        This work is based on the concept of <a href="http://ben-major.co.uk/2013/12/uk-top-40-charts-api/">Ben Major</a>,
        who published a PHP-based chart which scraped  the BBC chart web page for
        <a href="http://www.bbc.co.uk/radio1/chart/singles/print">singles</a>
        and <a href="http://www.bbc.co.uk/radio1/chart/singles/print">albums</a>.
        <p>
            Ben's code scrapes the BBC's site in realtime for every visit. The problem is that the BBC's
            site appears to give occasional 500 errors, and as a result Ben's API is not itself always
            available.
        </p>
        <p>
            As I plan to give access to this API to school children, I needed to try to make the API more
            robust. In fairness to Ben, this was never a design goal for him.
        </p>
        <p>
            As a result, <strong>PythonTop40Server</strong> caches the scraped results for a specific time
            (at the moment that is an hour) before re-reading from the BBC site. If for whatever reason
            the BBC site is down, <strong>PythonTop40Server</strong> will simply return the last valid
            version from its internal MongoDB database. So <em>in theory</em> the API should
            always return a valid response - albeit one that <em>might</em> be <em>slightly</em> out of
            date.
        </p>
        <h4>V1 and V2 Differences</h4>
        <p>
            The API can be found in two versions as described above.
            <span class="label label-default">V1</span> mirrors
            <a href="http://ben-major.co.uk/2013/12/uk-top-40-charts-api/"> Ben Major's API</a>
            <em>nearly</em> exactly, whereas
            <span class="label label-default">V2</span> features a couple of extensions.
        </p>
        <p>
            Specifically, <span class="label label-default">V2</span> returns a <code>status</code>
            value that describes the movement of the chart entry since the last chart. The <code>status</code>
            value comes straight from the BBC's chart page and is returned without any modification.
        </p>
        <p>
            <span class="label label-default">V2</span> also returns a <code>current</code> flag that,
            if <code>true</code>, means that the results have come from a scraping of the BBC's web site
            within the last <em>n</em> minutes (n is currently 60). A <code>false</code> value would
            mean that the chart could not be returned from the BBC's web page, and a previous version that
            is older than <em>n</em> minutes is being served.
        </p>
        <p>
            Importantly, and somewhat confusingly, this does not mean that the chart is
            <em>not current</em>. As the chart only changes once per week, it's highly likely that the
            chart is still accurate, but the <code>current</code> flag serves to show that there was a
            problem reading from the BBC's web site.
        </p>
        <p>
            A <span class="label label-default">V2</span> chart header showing the <code>status</code> and
            <code>current</code> fields, together with a single chart entry might look like this:
        </p>
        <p>
<pre class="prettyprint"><code class="language-json">
{
"current": true,
"date": 1417910400,
"retrieved": 1418373750
"entries": [
{
  "artist": "Take That",
  "change": {
    "actual": 40,
    "amount": 40,
    "direction": "up"
  },
  "numWeeks": 1,
  "position": 1,
  "previousPosition": 0,
  "status": "new",
  "title": "III"
},
.
.
.
]
}</code></pre>
        </p>
        <p>
            A small difference exists between Ben's API and my <span class="label label-default">V1</span>
            implementation. When the <code>change</code> structure is being calculated
            <em>for new chart entries</em>, I compute the <code>actual</code> and <code>amount</code> fields
            differently from Ben's API.
            For the <code>actual</code>
            field, Ben has decided to simply return the difference between <code>position</code> and
            <code>previousPosition</code>. For new entries that have a <code>previousPosition</code> of 0,
            this can mean that the <change>amount</change> field is negative when in fact the direction of
            movement has been positive.
        </p>
        <p>
            I took the view that for new entries (entries that have a <code>previousPosition</code> of
            0) I should describe the movement from position 41. Therefore, just
            as in the JSON extract above, if an entry goes straight in at number 1, I will describe the
            change as 40 places. Given that my interface will be being used by school children, I think
            this is a less confusing implementation - for my purposes.
        </p>
        <h4>To Do</h4>
        <p>
            As the UK Top 40 charts are only published once per week on a Sunday, it would make sense to
            cache the results until just before the new chart is available. I will change the way the cache
            is created in a future version.

        </p>
    </div>
    <div class="col-md-8">
        <h3>Singles Chart for w/c {{ singles_date}}</h3>
        <table class="table  table-striped table-condensed">
            <caption>(Retrieved @{{ singles_retrieved }})</caption>
            <thead>
                <tr>
                <th>#</th>
                <th>Title</th>
                <th>Artist</th>
                </tr>
            </thead>
            <tbody>
                {% for entry in singles_chart.entries %}
                    <tr>
                        <td>{{ entry.position }}</td>
                        <td>{{ entry.title }}</td>
                        <td>{{ entry.artist }}</td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>

        <h3>Albums Chart for w/c {{ albums_date}}</h3>
        <table class="table  table-striped table-condensed">
            <caption>(Retrieved @{{ albums_retrieved }})</caption>
            <thead>
                <tr>
                <th>#</th>
                <th>Title</th>
                <th>Artist</th>
                </tr>
            </thead>
            <tbody>
                {% for entry in albums_chart.entries %}
                    <tr>
                        <td>{{ entry.position }}</td>
                        <td>{{ entry.title }}</td>
                        <td>{{ entry.artist }}</td>
                    </tr>
                {% endfor %}
            </tbody>
        </table>
    </div>
</div>
{% endblock %}

{% block styles %}
{{ super() }}
        <link rel="stylesheet" href="/static/css/sticky-footer.css">
        <link rel="stylesheet" href="/static/css/prettify.css">
{% endblock %}

{% block scripts %}
    {{ super() }}
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=py&lang=json&skin=sunburst"></script>
    <script>
        // Activate Google Prettify in this page
        addEventListener('load', prettyPrint, false);
        $(document).ready(function(){
            // Add prettyprint class to pre elements
            $('pre').addClass('prettyprint');
        }); // end document.ready
    </script>
{% endblock %}

<!DOCTYPE html>
<html lang="en">
    <body>

        <!-- Include some JavaScript scripts -->
        <!-- JQuery JavaScript required for SOME Bootstrap features-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <!--<script src="/static/js/prettify.js"></script>-->

  </body>
</html>