# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import Flask
from flask.ext import restful
from flask.ext.pymongo import PyMongo
from flask_bootstrap import Bootstrap
import logbook
from logbook import NullHandler, StderrHandler, NestedSetup, Logger
from pythontop40server.errors import Top40ScrapingError

from pythontop40server.utils import env_value
from pythontop40server.config import DEBUG, CACHE_SECONDS, ON_HEROKU, MONGO_URI

# Create the Flask App
app = Flask(__name__)

# Turn debug mode on or off
app.config["DEBUG"] = DEBUG

# Tell Flask-PyMongo how to access the MongoDB
app.config["MONGO_URI"] = MONGO_URI

# Create our REST API instance
api = restful.Api(app)

# Create an instance of our MongoDB client. Only connect to Mongo if we are ON_HEROKU
mongo = PyMongo(app) if ON_HEROKU else None

# Let's create an error handler
error_handlers = NestedSetup(
    [
        NullHandler(),
        StderrHandler(level=logbook.NOTICE if not app.config['DEBUG'] else logbook.DEBUG)
    ]
)
log = Logger("PythonTop40Server", level=logbook.DEBUG if DEBUG else logbook.NOTICE)

# Initialise Bootstrap templates
bootstrap = Bootstrap(app)

import pythontop40server.views