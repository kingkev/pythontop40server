# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


from setuptools import find_packages, setup
import json
from os import path

PACKAGE_DIR = "pythontop40server"
LONG_DESCRIPTION_FILE = "README.rst"
PROJECT_DIR = path.abspath(path.dirname(__file__))
# Get the long description from the relevant file
with open(path.join(PROJECT_DIR, LONG_DESCRIPTION_FILE), encoding='utf-8') as f:
    long_description = f.read()

with open(
        '{}/package_info.json'.format(PACKAGE_DIR)
) as fp:
    _info = json.load(fp)

setup(
    name=_info['name'],
    version=_info['version_full'],
    packages=find_packages(exclude=['tests']),
    url=_info['url'],
    license=_info['license'],
    author=_info['author'],
    author_email=_info['author_email'],
    description=_info['description'],
    long_description=long_description,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.3',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ],
    install_requires=[
        "flask==0.10.1",
        "flask-restful==0.3.0",
        "flask-PyMongo==0.3.0",
        "flask-bootstrap==3.3.0.1",
        "requests==2.5.0",
        "beautifulsoup4==4.3.2",
        "html5lib",
        "arrow==0.4.4",
        "gunicorn==19.1.1",
        "pythontop40==0.1.0",
        "booby==0.7.0",
        "six==1.8.0",
        "logbook==0.8.1"
    ],
    dependency_links=[
    ]
)
