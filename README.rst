PythonTop40Server
=================

The **PythonTop40Server** library is designed to serve requests from the `pythontop40 module
<https://bitbucket.org/dannygoodall/pythontop40>`_ to return the UK Top 40 singles and albums. It was designed to return
the same data as the `version by Ben Major <http://ben-major.co.uk/2013/12/uk-top-40-charts-api/>`_
- from which it took its inspiration.

Changes
=======

v0.1.1 - 15th January 2015
-------------------------------
* Ensure responses are utf-8 encoded

v0.1.0 - 8th December 2014
-------------------------------
* Initial non-dev version
* Now hosted on `Heroku <http://pythontop40server.herokuapp.com>`_

v0.1.0.dev1 - 8th December 2014
-------------------------------
* Initial version
