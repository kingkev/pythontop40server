# -*- coding: utf-8 -*-
#
# Copyright 2014 Danny Goodall
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import arrow

__author__ = 'User'

from expects import *
from pythontop40server.helpers import get_change_dict, scrape_bbc_page, strip_number_suffix, get_page_date

class TestGetChangeDict:
    def setUp(self):
        pass

    def test_should_fail_if_up_change_is_not_returned(self):
        expect(get_change_dict(11,16)).to(have_keys(direction="up",amount=5,actual=5))

    def test_should_fail_if_up_change_is_not_returned_on_new_entry(self):
        expect(get_change_dict(11,0)).to(have_keys(direction="up",amount=30,actual=30))

    def test_should_fail_if_down_change_is_not_return(self):
        expect(get_change_dict(16,11)).to(have_keys(direction="down",amount=5,actual=-5))

    def test_should_fail_if_none_change_is_not_return(self):
        expect(get_change_dict(11,11)).to(have_keys(direction="none",amount=0,actual=0))


class TestStripNumberSuffix:
    def setUp(self):
        pass

    def test_should_fail_if_th_is_not_stripped(self):
        expect(strip_number_suffix("4th November 2014")).to(equal("4 November 2014"))

    def test_should_fail_if_rd_is_not_stripped(self):
        expect(strip_number_suffix("3rd November 2014")).to(equal("3 November 2014"))

    def test_should_fail_if_nd_is_not_stripped(self):
        expect(strip_number_suffix("2nd November 2014")).to(equal("2 November 2014"))

    def test_should_fail_if_st_is_not_stripped(self):
        expect(strip_number_suffix("1st November 2014")).to(equal("1 November 2014"))

    def test_should_fail_if_two_digit_th_is_not_stripped(self):
        expect(strip_number_suffix("24th November 2014")).to(equal("24 November 2014"))

    def test_should_fail_if_two_digit_rd_is_not_stripped(self):
        expect(strip_number_suffix("23rd November 2014")).to(equal("23 November 2014"))

    def test_should_fail_if_two_digit_nd_is_not_stripped(self):
        expect(strip_number_suffix("22nd November 2014")).to(equal("22 November 2014"))

    def test_should_fail_if_two_digit_st_is_not_stripped(self):
        expect(strip_number_suffix("21st November 2014")).to(equal("21 November 2014"))

    def test_should_fail_if_space_between_number_and_suffix(self):
        expect(strip_number_suffix("21 st November 2014")).to(equal("21 st November 2014"))


class TestGetPageDate:
    def setUp(self):
        pass

    def test_should_fail_if_date_not_extracted_correctly(self):
        expect(get_page_date("The Official UK Top 40 Albums Chart - 30th November 2014")).to(
            equal(arrow.Arrow(2014,11,30))
        )
        expect(get_page_date("The Official UK Top 40 Albums Chart - 23rd January 2015")).to(
            equal(arrow.Arrow(2015,1,23))
        )
        expect(get_page_date("The Official UK Top 40 Albums Chart - 21st July 2016")).to(
            equal(arrow.Arrow(2016,7,21))
        )
        expect(get_page_date("The Official UK Top 40 Albums Chart - 22nd February 2017")).to(
            equal(arrow.Arrow(2017,2,22))
        )
